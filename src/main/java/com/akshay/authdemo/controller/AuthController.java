package com.akshay.authdemo.controller;


import com.akshay.authdemo.model.Request;
import com.akshay.authdemo.utils.AuthenticationHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

@RestController
public class AuthController {

	String username_fromDb="akshay";
	String password_fromDb="password";

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private UserDetailsService jwtInMemoryUserDetailsService;


	@RequestMapping(value="/login",method=RequestMethod.POST)
	public ResponseEntity<String> generateToken( @RequestBody Request authenticationRequest ){
		String token=null;

		final UserDetails userDetails = jwtInMemoryUserDetailsService
				.loadUserByUsername(authenticationRequest.getUsername());

		authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(),
				authenticationRequest.getPassword()));

		token = new AuthenticationHandler().generateToken(userDetails.getUsername());

		return new ResponseEntity<>(token,HttpStatus.OK);
	}




	@RequestMapping(value="/hello",method=RequestMethod.GET)
	public ResponseEntity<String> helloMsg( ){

		return new ResponseEntity<>("Hello world",HttpStatus.OK);
	}
	
	
	

}
