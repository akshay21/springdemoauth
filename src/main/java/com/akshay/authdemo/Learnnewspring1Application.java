package com.akshay.authdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;


@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class,
		 DataSourceTransactionManagerAutoConfiguration.class,
		 SecurityAutoConfiguration.class,
		 
		 
		} )
public class Learnnewspring1Application {

	public static void main(String[] args) {
		SpringApplication.run(Learnnewspring1Application.class, args);
	}

}
