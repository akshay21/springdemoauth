package com.akshay.authdemo.utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateUtils {
	 
	public static Session getSessionFactory() {
		
		 try {
			 Configuration config = new Configuration();
//			 config.addResource("hibernate.cfg.xml");
			 config.configure();
			 StandardServiceRegistry serviceregbuilder = new StandardServiceRegistryBuilder().applySettings(config.getProperties()).build();
					 
					 
			 SessionFactory sessionfac = config.buildSessionFactory(serviceregbuilder);

			 SessionFactory sf = new Configuration().configure().buildSessionFactory();

			 return  sessionfac.openSession();
			 
			 
		 }catch(Exception e) {
			 
			 e.printStackTrace();
			 
		 }
		
		
		return null;
		
		
	}

}
