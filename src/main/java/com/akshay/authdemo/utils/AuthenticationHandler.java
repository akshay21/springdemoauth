package com.akshay.authdemo.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;
import java.util.HashMap;

public class AuthenticationHandler {

    /// create token
    /// validate token with exp
    /// create secret key

    public static final long tokenValidity = 5 * 60 * 60;
    private String secretKey="akshay";


    public String generateToken(String username){

        System.out.println("Token generated");
        Date date = new Date();
        long t = date.getTime();
        int sec= 60;// set 30 seconds
        Date expirationTime = new Date(t + sec*1000l);



        return Jwts.builder().setClaims(new HashMap<String,Object>())
                .setSubject(username).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(expirationTime )
                .signWith(SignatureAlgorithm.HS256,secretKey).compact();
    }





    public String validateToken(String token){

        Claims claims =null ;

        try {
            claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();

        } catch (IllegalArgumentException e) {
            System.out.println("Unable to get JWT Token");
        } catch (ExpiredJwtException e) {
            System.out.println("JWT Token has expired");
        }

       return  claims.getSubject();
    }

}
